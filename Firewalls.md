# A Beginner’s Guide To Firewall


<img src="https://www.hostduplex.com/wp-content/uploads/2018/03/FireWall-1.png" alt="Introduction of firewall" class="landscape full" width="600" height="250">

In this article, we will discuss about firewall and its types. We will also discuss about the need of firewall in today's world.

# What is Firewall?
In network, A firewall is basically a network security system — that can help to protect our network by filtering traffic and blocking outsiders based on predetermined security rules from gaining unauthorized access to the private data on our computer. It can also help block malicious software from infecting our computer. Simply we can say firewall is a filter which filter network traffics based on predetermined security rules. It is not that the firewall can do just blocking outsiders, blocking malicious software etc. Firewalls can provide different levels of protection. The key is determining how much protection we need. Firewall itself can be hardware or software, it can also consist of a combination of both. 


The term firewall originally referred to a wall intended to restrict fire within a line of adjacent buildings. Firewall acts as a shield to the outside world for our computer. Lets take an example of one house that has four sides. It must have a firewall at all four sides to restrict fire within a line of adjacent buildings. If one side is not limited by a firewall while the other three are fitted with a firewall, it will be pointless to try to hold back the fire that will spread quickly. Similarly, this rule is applied on computer firewalls too. Firewall must have to follow some rules & regulation or we can say standards to establish a ‘security fence’ around a private network, prevent unauthorized access and the private data on the user's computer. In the market, we have different types of firewalls whose functionality is different from each other. The difference between firewalls with one another is usually in security measures, selectivity of access and the scope of protection at different level.

# Why do we need a firewall?
As we all know that nowadays we store our private data on our computers or mobiles or drives or on any platform. Computers, laptops and other devices that are connected to the Internet are exposed to the possibility of being attacked or harmed and potential target of a range of different threats, such as malwares,  keyloggers, and Trojans that attack through unpatched security holes. This can make you a victim of identity theft or other attacks if you are a frequent Internet banker or shopper with a poor security system or a malfunctioning firewall.

To avoid all these things, we can take help of firewall. Firewall will act as a shield, between your PC and Internet. When we are connected to the Internet, we are constantly sending and receiving information in small units called packets. The firewall filters these packets to see if they meet certain criteria set by a series of rules, and thereafter blocks or allows the data. This way, hackers cannot get inside and steal information such as bank account numbers and passwords from us.

# What are the types of Firewall?
Firewalls are divided into two category:
1. Network-based system
2. Host-based system

    <img src="https://i2.wp.com/ipwithease.com/wp-content/uploads/2018/01/153-network-based-firewall-vs-host-based-firewall-01.png?resize=1060%2C469&amp;ssl=1" alt="Different Types of Firewalls" class="landscape full" width="600" height="250">

# Network-based system firewall
A network-based firewall controls traffic going in and out of a network. It performs this traffic control by filtering traffic based on predetermined security rules and allows only authorized traffic to pass through it. Most of the companies install one network-based firewall at the boundary between their internal network and the Internet. It is hardware based firewall. Performance of Network-based system firewall is high as compared to Host-based system firewall. Amazon's firewall in AWS environments is an example of Network-based system firewall.

# Host-based system firewall
A host-based firewall is a piece of firewall software on a single host that can restrict incoming and outgoing network activity for that host only. They can prevent a host from becoming infected and stop infected hosts from spreading malware to other hosts. Host-based system firewall is very helpful to protect the individual hosts from viruses and malware, and to control the spread of these harmful infections throughout the network. Windows defender is one of the example of Host-based firewall.

With this, we come to an end to this blog. I hope you got a clear understanding about Firewall and its types from this article.
